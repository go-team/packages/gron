gron (0.7.1-2) unstable; urgency=medium

  * d/clean:
    - Remove generated manpage and CHANGELOG files (Closes: #1044498)
  * d/control:
    - Declare compliance with Debian Policy 4.6.2
  * d/copyrght:
    - Update years of Debian copyright
  * d/gron.lintian-overrides:
    - Ignore false positive typo in binary
  * d/patches:
    - Add patch drop-shebang-from-fish-completion
  * d/s/lintian-overrides:
    - Add override for long line in test data
  * d/rules:
    - Inject upstream version number into source before build
    - Install completions for fish shell

 -- Nick Morrott <nickm@debian.org>  Sun, 25 Feb 2024 05:19:14 +0000

gron (0.7.1-1) unstable; urgency=medium

  [ Aloïs Micard ]
  * d/gitlab-ci.yml:
    - Update using pkg-go-tools/ci-config

  [ Nick Morrott ]
  * New upstream version 0.7.1
  * d/control:
    - Declare compliance with Debian Policy 4.6.0
  * d/copyright:
    - Refresh years of Debian copyright
  * d/gron.1.md.in:
    - Refresh manpage for new version

 -- Nick Morrott <nickm@debian.org>  Sun, 24 Apr 2022 04:29:46 +0100

gron (0.6.1-1) unstable; urgency=medium

  * New upstream version 0.6.1
  * d/control:
    - Declare compliance with Debian Policy 4.5.1
    - Bump debhelper compatibility level to 13
    - Add Rules-Requires-Root field, set to no
    - Update Uploaders email address
  * d/copyright:
    - Refresh years of Debian copyright and maintainer email address
  * d/gron.1.md.in:
    - Correct typos copied from upstream documentation
  * d/patches:
    - Add patch typo-in-manual-page
    - Drop patch update-upstream-changelog-to-current (applied upstream)

 -- Nick Morrott <nickm@debian.org>  Sun, 07 Feb 2021 14:52:30 +0000

gron (0.6.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 04 Feb 2021 17:56:57 +0000

gron (0.6.0-1) unstable; urgency=medium

  * Initial release (Closes: #894914)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Mon, 21 Jan 2019 14:00:00 +0000
